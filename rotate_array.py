#!/usr/bin/env python3

#Task:
#Given a list, right rotate the list by k position.

def solution(A, k):
    a_len = len(A)
    if a_len <= 1:
        return A
    elif k == a_len:
        return A
    elif k >= a_len:
        k = (k - ((k // a_len) * a_len))
    while k > 0:
        last = A.pop()
        A.insert(0, last)
        k -= 1
    return (A)


test_cases =[[[1,2,3,4,5,6,7], 3], [[-1,-100,3,99], 2], [[-1],4], [[1,2,3,4,5,6,7], 16]]
for i in test_cases:
    res = solution(i[0], i[1])
    print (res)