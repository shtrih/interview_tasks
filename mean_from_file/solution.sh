#!/usr/bin/env bash

min=$(sort -t= -k2 -n file.txt | head -1 | cut -d"=" -f2)
max=$(sort -t= -k2 -n file.txt | tail -1 | cut -d"=" -f2)
values=$(cut -d"=" -f2 file.txt | sort | uniq)
count=$(echo $values | wc -w)
for i in $values; do
	sum=$((sum+i))
done

echo  -e "Min: \n$(grep =$min file.txt)"
echo -e "Max:  \n$(grep =$max file.txt)"
echo "Mean: $((sum/count))"