#!/usr/bin/env python3

with open("file.txt") as f:
	d = dict(x.rstrip().split("=") for x in f)
values = [ int(x) for x in d.values() ]
uniq_values = list(dict.fromkeys(values))

for key, value in d.items():
	if int(value) == min(values):
		print ("Min: {}={}".format(key,value))
	elif int(value) == max(values):
		print ("Max: {}={}".format(key,value))

print ("Mean: {}".format(sum(uniq_values)/len(uniq_values)))


