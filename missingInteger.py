#!/usr/bin/env python3

def solution(A):
    if len(A) <= 1:
        if A[0] < 1:
            return 1
        elif A[0] == 1:
            return 2
        elif A[0] >1:
            return A[0]-1
    elif 1 not in A:
        return 1
    smallest = 1 if min(A) <=0 else min(A)
    biggest = max(A)
    test = set(range(1, biggest))
    diff = test - set(A)
    if diff:
        return min(diff)
    else:
        return biggest + 1

#Too slow solution
#    while smallest in A:
#        smallest +=1
#    print ("correct: %s" % smallest)


test_cases = [[1, 3, 6, 4, 1, 2], [1, 2, 3], [-1, -3], [2], [1], [4, 5, 6, 2]]

for i in test_cases:
    res = solution(i)
    print (res)

